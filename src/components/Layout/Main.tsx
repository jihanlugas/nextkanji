import { ReactNode, useEffect } from "react";
import type { NextPage } from "next";
import Head from "next/head";
import Header from "./Header";
import Sidebar from "./Sidebar";
import { useState } from "react";
import { useQuery } from "react-query";
import { Api } from "../../lib/Api";


type Props = {

};

const Main: NextPage<Props> = ({ children }) => {

    const [show, setShow] = useState(false);

    const onClickOverlay = () => {
        setShow(!show)
    }

    // const { isLoading, error, data, isFetching } = useQuery("me", () => Api.get("/devapi/user/me"));
    const { isLoading, error, data, isFetching } = useQuery("me", () => Api.get("/user/me"));

    return (
        <main className={""}>
            <Sidebar onClickOverlay={onClickOverlay} show={show} />
            <Header onClickOverlay={onClickOverlay} />
            {children}
        </main>
    )
}

export default Main