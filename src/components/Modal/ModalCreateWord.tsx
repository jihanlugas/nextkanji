import { useContext, useState, useEffect } from 'react'
import { NextPage } from "next";
import AppContext from "@stores/appContext"
import * as Yup from 'yup';
import { Form, Formik, FormikValues, FieldArray } from 'formik';
import { useMutation } from 'react-query'
import Modal from '@com/Modal/Modal';
import TextField from "@com/formik/TextField";
import TextAreaField from "@com/formik/TextAreaField";
import ButtonSubmit from "@com/formik/ButtonSubmit";
import { Api } from '@lib/Api';
import { AiOutlineLoading3Quarters } from 'react-icons/ai'
import { BsTrash } from 'react-icons/bs'


interface Props {
    show: boolean;
    onClickOverlay: Function;
    selectedId: number;
}

interface Init {
    wordId: number,
    word: string,
    kana: string,
    means: string[],
    description: string,
}

let schema = Yup.object().shape({
    word: Yup.string().required(),
    kana: Yup.string().required(),
    means: Yup.array().of(Yup.string().required()),
    description: Yup.string(),
});

const defaultInit = {
    wordId: 0,
    word: "",
    kana: "",
    means: [""],
    description: "",
}

const ModalCreateWord: NextPage<Props> = ({ show, onClickOverlay, selectedId }) => {
    const { notif } = useContext(AppContext)
    const [init, setInit] = useState<Init>(defaultInit);

    const FormSave = useMutation((val: FormikValues) => Api.post("/word/form", val))
    const FormUpdate = useMutation((val: FormikValues) => Api.put("/word/update", val))
    const FormReq = useMutation((id: number) => Api.get("/word/" + id))
    const FormDelete = useMutation((id: number) => Api.delete("/word/" + id))

    const handleSubmit = (values: FormikValues, setErrors, resetForm) => {
        if (selectedId === 0) {
            FormSave.mutate(values, {
                onSuccess: (res) => {
                    resetForm()
                    setInit(defaultInit)
                    notif.success(res.message)
                    onClickOverlay(0, true)
                }
            })
        } else {
            FormUpdate.mutate(values, {
                onSuccess: (res) => {
                    resetForm()
                    setInit(defaultInit)
                    notif.success(res.message)
                    onClickOverlay(0, true)
                }
            })
        }
    }

    const handleDelete = (resetForm) => {
        FormDelete.mutate(selectedId, {
            onSuccess: (res) => {
                resetForm()
                setInit(defaultInit)
                notif.success(res.message)
                onClickOverlay(0, true)
            }
        })
    }

    useEffect(() => {
        if (selectedId === 0) {
            setInit(defaultInit)
        } else {
            FormReq.mutate(selectedId, {
                onSuccess: (res) => {
                    console.log("res ", res)
                    setInit(res.payload)
                }
            })
        }
    }, [selectedId])

    return (
        <Modal show={show} onClickOverlay={onClickOverlay}>
            <div className={"p-4"}>
                {FormReq.isLoading ? (
                    <div className={"h-60 flex justify-center items-center"}>
                        <AiOutlineLoading3Quarters className={"animate-spin"} size={"4em"} />
                    </div>
                ) : (
                    <Formik
                        initialValues={init}
                        validationSchema={schema}
                        enableReinitialize={true}
                        onSubmit={(values, { setErrors, resetForm }) => handleSubmit(values, setErrors, resetForm)}
                    >
                        {({ values, resetForm }) => {
                            return (
                                <Form>
                                    <div>
                                        <div className={"flex justify-between items-center mb-4"}>
                                            <span className={"text-xl"}>Word</span>
                                            {selectedId !== 0 && (
                                                <div className={"flex justify-center items-center h-8 w-8"} onClick={() => (handleDelete(resetForm))} >
                                                    <BsTrash className={"text-red-600"} size={"1em"} />
                                                </div>
                                            )}
                                        </div>
                                        <div className="mb-4">
                                            <TextField
                                                label={"Word"}
                                                name={"word"}
                                                type={"text"}
                                                placeholder={"Word"}
                                            />
                                        </div>
                                        <div className="mb-4">
                                            <TextField
                                                label={"Kana"}
                                                name={"kana"}
                                                type={"text"}
                                                placeholder={"Kana"}
                                            />
                                        </div>
                                        <FieldArray
                                            name={"means"}
                                        >
                                            {(arrayHelpers) => {
                                                return (
                                                    <div className="">
                                                        {values.means.map((mean, key) => {
                                                            return (
                                                                <div className="mb-4 flex items-center" key={key}>
                                                                    <TextField
                                                                        label={`Mean ${key + 1}`}
                                                                        name={`means[${key}]`}
                                                                        type={"text"}
                                                                        placeholder={"Mean"}
                                                                    />
                                                                </div>
                                                            )
                                                        })}
                                                        <div className={"flex justify-end mb-4"}>
                                                            <div className={"text-blue-500"} onClick={() => arrayHelpers.push("")}>
                                                                <span>Add Another Mean</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            }}
                                        </FieldArray>
                                        <div className="mb-4">
                                            <TextAreaField
                                                label={"Description"}
                                                name={"description"}
                                                type={"text"}
                                                placeholder={"Description"}
                                            />
                                        </div>
                                        <div className={""}>
                                            <ButtonSubmit
                                                label={'Save'}
                                                disabled={FormSave.isLoading || FormUpdate.isLoading}
                                                loading={FormSave.isLoading || FormUpdate.isLoading}
                                            />
                                        </div>
                                    </div>
                                </Form>
                            )
                        }}
                    </Formik>
                )}
            </div>
        </Modal>
    )
}

export default ModalCreateWord