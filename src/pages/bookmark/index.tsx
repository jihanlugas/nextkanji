import type { NextPage } from "next"
import Head from "next/head"
import Main from "../../components/Layout/Main"
import { useMutation, useQuery } from "react-query";
import { Api } from "../../lib/Api";
import { useEffect, useState } from "react";

interface Props {

}

const Bookmark: NextPage<Props> = ({ }) => {

    const { data, mutate } = useMutation(() => Api.get("/devapi/kanji"))
    const [userkanjis, setUserkanjis] = useState<[]>([])


    useEffect(() => {
        mutate(null, {
            onSuccess: (res) => {
                setUserkanjis(res.data)
            }
        })
    }, [])

    useEffect(() => {
        console.log("data ", data)
    }, [data])

    return (
        <Main>
            <Head>
                <title>Bookmark</title>
            </Head>
            <div className={"p-4 font-sans w-full max-w-4xl mx-auto"}>
                <div className={"mb-4"}>
                    <div className={"text-2xl"} >Bookmark</div>
                </div>
                <div className={"mb-4"}>
                    {userkanjis.map((val: { kanji: any }, key) => {
                        return (
                            <div className={"mb-4"} key={key}>
                                {val.kanji}
                            </div>
                        )
                    })}
                </div>
            </div>
        </Main>
    )
}

export default Bookmark