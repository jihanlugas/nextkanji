import { NextPage } from "next"
import Head from "next/head"
import Main from "@com/Layout/Main"
import { Api } from "../../lib/Api";
import { useQuery } from "react-query";
import { useState } from "react";
import { useEffect } from "react";
import { AiOutlineLoading3Quarters, AiOutlineUnorderedList } from 'react-icons/ai'
import { RiGridFill } from 'react-icons/ri'
import Link from 'next/link'


interface Props {

}

const defaultParam = {
    "heisigEn": "",
    "kanji": "",
    "unicode": ""
}

interface Ikanji {
    kanjiId: number,
    kanji: string,
    jlpt: number,
    grade: string,
    heisigEn: string,
    kunReadings: string[],
    meanings: string[],
    nameReadings: string[],
    onReadings: string[],
    unicode: string,
    strokeCount: number,
}

const viewType = [
    {
        "name": "Grid",
        "icon": <RiGridFill className={""} size={"2em"} />,
    },
    {
        "name": "Flex",
        "icon": <AiOutlineUnorderedList className={""} size={"2em"} />,
    },
]


interface Ikanjis extends Array<Ikanji> { }

const Kanji: NextPage<Props> = () => {

    const [param, setParam] = useState(defaultParam)
    const [kanjis, setKanjis] = useState<Ikanjis>([])
    const [interval, setInterval] = useState<number>(0)
    const [kanjiview, setKanjiview] = useState(interval)
    const { isLoading, error, data, isFetching } = useQuery("kanji", () => Api.post("/userkanji/list", param));


    useEffect(() => {
        if (data && data.payload) {
            setKanjis(data.payload)
        } else {
            setKanjis([])
        }
    }, [data])

    useEffect(() => {
        setKanjiview(interval % viewType.length)
    }, [interval])

    return (
        <Main>
            <Head>
                <title>Kanji</title>
            </Head>
            <div className={"p-4 font-sans w-full max-w-4xl mx-auto"}>
                <div className={"mb-4 flex justify-between items-center"}>
                    <div className={"text-2xl"} >Kanji</div>
                    <div className={"w-12 h-12 flex justify-center items-center"} onClick={() => setInterval(interval + 1)}>{viewType[kanjiview].icon}</div>
                </div>

                {isLoading ? (
                    <div className={"flex justify-center p-12"}>
                        <AiOutlineLoading3Quarters className={"animate-spin"} size={"4em"} />
                    </div>
                ) : (
                    <div className={"mb-4"}>
                        {kanjis.length === 0 ? (
                            <div>No Data</div>
                        ) : (
                            <div>
                                {kanjiview === 0 ? (
                                    <div className={"grid grid-cols-3 gap-4"}>
                                        {kanjis.map((kanji, key) => {
                                            return (
                                                <Link key={key} href={`/kanji/${kanji.kanji}`} passHref >
                                                    <div className="flex flex-col justify-center items-center shadow-lg p-2 rounded-lg bg-gray-200">
                                                        <div className={"w-12 h-12 font-kanji text-3xl flex justify-center items-center"}>{kanji.kanji}</div>
                                                        <div>{kanji.heisigEn}</div>
                                                    </div>
                                                </Link>
                                            )
                                        })}
                                    </div>
                                ) : kanjiview === 1 ? (
                                    <div className={"flex flex-col"}>
                                        {kanjis.map((kanji, key) => {
                                            return (
                                                <Link key={key} href={`/kanji/${kanji.kanji}`} passHref >
                                                    <div className="flex justify-start items-center mb-2">
                                                        <div className={"w-12 font-kanji text-2xl flex flex-shrink-0 justify-center items-center"}>{kanji.kanji}</div>
                                                        <div className={"flex flex-auto flex-col border-b min-h-12"}>
                                                            <div className={"flex flex-auto flex-wrap"}>
                                                                {kanji.onReadings.map((onReading, key) => {
                                                                    return (
                                                                        <div key={key} className={"px-2 bg-green-400 text-gray-50 text-xs mr-2 font-bold rounded-md mb-2"}>
                                                                            {onReading}
                                                                        </div>
                                                                    )
                                                                })}
                                                            </div>
                                                            <div className={"flex flex-wrap"}>
                                                                {kanji.kunReadings.map((kunReading, key) => {
                                                                    return (
                                                                        <div key={key} className={"px-2 bg-blue-400 text-gray-50 text-xs mr-2 font-bold rounded-md mb-2"}>
                                                                            {kunReading}
                                                                        </div>
                                                                    )
                                                                })}
                                                            </div>
                                                            <div>{kanji.heisigEn}</div>
                                                        </div>
                                                    </div>
                                                </Link>
                                            )
                                        })}
                                    </div>
                                ) : (
                                    <div className={""}>
                                        2
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                )}

            </div>
        </Main >
    )
}

export default Kanji