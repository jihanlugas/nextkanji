import { NextPage, NextPageContext } from "next"
import Head from "next/head"
import Main from "@com/Layout/Main"
import { Api } from "../../lib/Api";
import { useQuery } from "react-query";
import { useState } from "react";
import { useEffect } from "react";
import { AiOutlineLoading3Quarters } from 'react-icons/ai'
import { BiChevronRight } from 'react-icons/bi'
import Link from 'next/link'


interface Props {
    // kanjiId: number,
}

interface Iword {
    wordId: number
    word: string,
    kana: string,
    means: string[],
    kanjis: string[],
    description: string,
}


interface Iwords extends Array<Iword> { }

interface Isentence {
    sentenceId: number
    sentence: string,
    kana: string,
    mean: string,
    kanjis: string[],
    description: string,
}

interface Isentences extends Array<Isentence> { }

interface Ikanji {
    kanjiId: number,
    kanji: string,
    jlpt: number,
    grade: string,
    heisigEn: string,
    kunReadings: string[],
    meanings: string[],
    nameReadings: string[],
    onReadings: string[],
    unicode: string,
    strokeCount: number,
    listWord: Iwords,
    listSentence: Isentences,
}


const Kanji = ({ qKanji }) => {

    const { isLoading, error, data, isFetching, refetch } = useQuery("kanji", () => Api.post("/userkanji/search", { kanji: qKanji }));

    const [kanji, setKanji] = useState<Ikanji>(null)

    useEffect(() => {
        if (data && data.payload) {
            setKanji(data.payload)
        } else {
            setKanji(null)
        }
    }, [data])

    useEffect(() => {
        refetch()
    }, [qKanji])

    return (
        <Main>
            <Head>
                <title>Kanji</title>
            </Head>
            <div className={"p-4 font-sans w-full max-w-4xl mx-auto"}>
                <div className={"mb-4 flex text-2xl items-center"}>
                    <Link href={"/kanji"} passHref>
                        <div className={"mr-2"} >Kanji</div>
                    </Link>
                    <BiChevronRight className={"mr-2"} size={"1em"} />
                    <div className={"mr-2"} >{kanji && kanji.kanji}</div>
                </div>

                {isLoading ? (
                    <div className={"flex justify-center p-12"}>
                        <AiOutlineLoading3Quarters className={"animate-spin"} size={"4em"} />
                    </div>
                ) : (
                    <div className={"mb-4"}>
                        {kanji ? (
                            <div className="flex flex-col">
                                <div className="flex mb-4">
                                    <div className={"w-20 h-20 flex justify-center items-center font-kanji text-5xl bg-gray-200 rounded-lg mr-4"}>
                                        {kanji.kanji}
                                    </div>
                                    <div className={"flex-auto"}>
                                        <div className={"grid grid-cols-3 gap-4 "}>
                                            <div className={"flex flex-col justify-center items-center p-2"}>
                                                <div className={"text-lg"}>{kanji.grade}</div>
                                                <div className={"text-sm font-bold"}>Grade</div>
                                            </div>
                                            <div className={"flex flex-col justify-center items-center p-2"}>
                                                <div className={"text-lg"}>{kanji.jlpt}</div>
                                                <div className={"text-sm font-bold"}>JLPT</div>
                                            </div>
                                            <div className={"flex flex-col justify-center items-center p-2"}>
                                                <div className={"text-lg"}>{kanji.strokeCount}</div>
                                                <div className={"text-sm font-bold"}>Strokes</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="border-b-2 py-2 mb-2">
                                    <div className={"text-lg"}>
                                        Meaning
                                    </div>
                                    {kanji.meanings.map((meaning, key) => {
                                        return (
                                            <div key={key}>
                                                {(key + 1) + ". " + meaning}
                                            </div>
                                        )
                                    })}
                                </div>
                                <div className="border-b-2 py-2 mb-2">
                                    <div className={"text-lg mb-2"}>
                                        On Readings
                                    </div>
                                    <div className={"flex flex-wrap"}>
                                        {kanji.onReadings.length !== 0 ? kanji.onReadings.map((onReading, key) => {
                                            return (
                                                <div key={key} className={"px-2 bg-green-400 text-gray-50 mr-2 font-bold rounded-md mb-2"}>
                                                    {onReading}
                                                </div>
                                            )
                                        }) : <div>-</div>}
                                    </div>
                                </div>
                                <div className="border-b-2 py-2 mb-2">
                                    <div className={"text-lg mb-2"}>
                                        Kun Readings
                                    </div>
                                    <div className={"flex flex-wrap"}>
                                        {kanji.kunReadings.length !== 0 ? kanji.kunReadings.map((kunReading, key) => {
                                            return (
                                                <div key={key} className={"px-2 bg-blue-400 text-gray-50 mr-2 font-bold rounded-md mb-2"}>
                                                    {kunReading}
                                                </div>
                                            )
                                        }) : <div>-</div>}
                                    </div>
                                </div>
                                <div className="border-b-2 py-2 mb-2">
                                    <div className={"text-lg mb-2"}>
                                        Name Readings
                                    </div>
                                    <div className={"flex flex-wrap"}>
                                        {kanji.nameReadings.length !== 0 ? kanji.nameReadings.map((nameReading, key) => {
                                            return (
                                                <div key={key} className={"px-2 bg-gray-700 text-gray-50 mr-2 font-bold rounded-md mb-2"}>
                                                    {nameReading}
                                                </div>
                                            )
                                        }) : <div>-</div>}
                                    </div>
                                </div>
                                {kanji.listWord && (
                                    <div className="border-b-2 py-2 mb-2">
                                        <div className={"text-lg"}>
                                            Words
                                        </div>
                                        {kanji.listWord.map((word, key) => {
                                            return (
                                                <div key={key} className="mb-4">
                                                    <div className={"text-xs"}>{word.kana}</div>
                                                    <div className={"text-lg "}>{word.word}</div>
                                                    {word.means.length !== 0 && (
                                                        <div className="mb-2">
                                                            {word.means.map((mean, key) => {
                                                                return (
                                                                    <div key={key}>
                                                                        <div>{(key + 1) + ". " + mean}</div>
                                                                    </div>
                                                                )
                                                            })}
                                                        </div>
                                                    )}
                                                    {word.kanjis.length !== 0 && (
                                                        <div className={"flex flex-wrap mb-2"}>
                                                            {word.kanjis.map((kanji, key) => {
                                                                return (
                                                                    <div key={key} className={"flex justify-center items-center w-10 h-10 shadow-md mr-4 rounded-sm text-lg bg-gray-200 mb-2"}>
                                                                        <Link key={key} href={`/kanji/${kanji}`} passHref>
                                                                            <div>{kanji}</div>
                                                                        </Link>
                                                                    </div>
                                                                )
                                                            })}
                                                        </div>
                                                    )}
                                                </div>
                                            )
                                        })}
                                    </div>
                                )}
                                {kanji.listSentence && (
                                    <div className="border-b-2 py-2 mb-2">
                                        <div className={"text-lg"}>
                                            Sentences
                                        </div>
                                        {kanji.listSentence.map((sentence, key) => {
                                            return (
                                                <div key={key} className="mb-4">
                                                    <div className={"text-xs"}>{sentence.kana}</div>
                                                    <div className={"text-lg"}>{sentence.sentence}</div>
                                                    <div className={"mb-2"}>{sentence.mean}</div>
                                                    {sentence.kanjis.length !== 0 && (
                                                        <div className={"flex flex-wrap"}>
                                                            {sentence.kanjis.map((kanji, key) => {
                                                                return (
                                                                    <div key={key} className={"flex justify-center items-center w-10 h-10 shadow-md mr-4 rounded-sm text-lg bg-gray-200 mb-2"}>
                                                                        <Link key={key} href={`/kanji/${kanji}`} passHref>
                                                                            <div>{kanji}</div>
                                                                        </Link>
                                                                    </div>
                                                                )
                                                            })}
                                                        </div>
                                                    )}
                                                </div>
                                            )
                                        })}
                                    </div>
                                )}
                            </div>
                        ) : (
                            <div>
                                <span>Data Not Found</span>
                            </div>
                        )}
                    </div>
                )}
            </div>
        </Main>
    )
}

Kanji.getInitialProps = async ({ query }: NextPageContext) => {
    return { qKanji: query.kanji }
}

export default Kanji