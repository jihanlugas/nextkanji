import { createProxyMiddleware } from "http-proxy-middleware";

export default createProxyMiddleware('', {
    target: process.env.API_END_POINT,
    changeOrigin: true,
    headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
    },
    pathRewrite: {
        '^/api/devapi': ''
    },
});

export const config = {
    api: {
        bodyParser: false
    }
}