import { NextPage } from "next"
import Head from "next/head"
import Main from "@com/Layout/Main"
import { Api } from "../../lib/Api";
import { useQuery } from "react-query";
import { useState } from "react";
import { useEffect } from "react";
import ModalCreateSentence from '@com/Modal/ModalCreateSentence';
import { AiOutlineLoading3Quarters } from 'react-icons/ai'
import Link from 'next/link'

interface Props {

}

const defaultParam = {
    "kana": "",
    "mean": "",
    "sentence": "",
    "description": "",
}

interface Isentence {
    sentenceId: number
    sentence: string,
    kana: string,
    mean: string,
    kanjis: string[],
    description: string,
}

interface Isentences extends Array<Isentence> { }

const Sentence: NextPage<Props> = () => {

    const [param, setParam] = useState(defaultParam)
    const [sentences, setSentences] = useState<Isentences>([])
    const [selectedId, setSelectedId] = useState<number>(0);
    const [show, setShow] = useState<boolean>(false);

    const { isLoading, error, data, isFetching, refetch } = useQuery("sentence", () => Api.post("/sentence/list", param));

    const onClickOverlay = (sentenceId: number = 0, refresh: boolean = false) => {
        setSelectedId(sentenceId)
        setShow(!show)
        if (refresh)
            refetch()
    }

    useEffect(() => {
        if (data && data.payload) {
            setSentences(data.payload)
        } else {
            setSentences([])
        }
    }, [data])

    return (
        <Main>
            <Head>
                <title>Sentence</title>
            </Head>
            <div className={"p-4 font-sans w-full max-w-4xl mx-auto"}>
                <div className={"mb-4 flex justify-between items-center"}>
                    <div className={"text-2xl"} >Sentence</div>
                    <div className={" bg-green-400 rounded flex items-center px-4 py-2"} onClick={() => onClickOverlay()}>
                        <div className={"text-white font-bold"} >Create</div>
                    </div>
                </div>
                {isLoading ? (
                    <div className={"flex justify-center p-12"}>
                        <AiOutlineLoading3Quarters className={"animate-spin"} size={"4em"} />
                    </div>
                ) : (
                    <div className={"mb-4"}>
                        {sentences.length === 0 ? (
                            <div>No Data</div>
                        ) : (
                            <div className={""}>
                                {sentences.map((sentence, key) => {
                                    return (
                                        <div key={key} className="border-b-2 py-4 mb-2" onClick={() => onClickOverlay(sentence.sentenceId)}>
                                            <div className={"text-xs"}>{sentence.kana}</div>
                                            <div className={"text-lg"}>{sentence.sentence}</div>
                                            <div className={"mb-2"}>{sentence.mean}</div>
                                            {sentence.kanjis.length !== 0 && (
                                                <div className={"flex flex-wrap mb-2"}>
                                                    {sentence.kanjis.map((kanji, key) => {
                                                        return (
                                                            <div key={key} className={"flex justify-center items-center w-10 h-10 shadow-md mr-4 rounded-sm text-lg bg-gray-200 mb-2"}>
                                                                <Link key={key} href={`/kanji/${kanji}`} passHref >
                                                                    <div>{kanji}</div>
                                                                </Link>
                                                            </div>
                                                        )
                                                    })}
                                                </div>
                                            )}
                                            <div className={"text-xs whitespace-pre-wrap"}>{sentence.description}</div>
                                        </div>
                                    )
                                })}
                            </div>
                        )}
                    </div>
                )}
                <ModalCreateSentence
                    show={show}
                    onClickOverlay={onClickOverlay}
                    selectedId={selectedId}
                />
            </div>
        </Main>
    )
}

export default Sentence