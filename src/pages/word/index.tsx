import { NextPage } from "next"
import Head from "next/head"
import Main from "@com/Layout/Main"
import { Api } from "../../lib/Api";
import { useQuery } from "react-query";
import { useState } from "react";
import { useEffect } from "react";
import ModalCreateWord from '@com/Modal/ModalCreateWord';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import Link from 'next/link';

interface Props {

}

const defaultParam = {
    "kana": "",
    "word": "",
    "description": "",
}

interface Iword {
    wordId: number
    word: string,
    kana: string,
    means: string[],
    kanjis: string[],
    description: string,
}

interface Iwords extends Array<Iword> { }

const Word: NextPage<Props> = () => {

    const [param, setParam] = useState(defaultParam)
    const [words, setWords] = useState<Iwords>([])
    const [selectedId, setSelectedId] = useState<number>(0);
    const [show, setShow] = useState<boolean>(false);


    const { isLoading, error, data, isFetching, refetch } = useQuery("word", () => Api.post("/word/list", param));

    const onClickOverlay = (wordId: number = 0, refresh: boolean = false) => {
        setSelectedId(wordId)
        setShow(!show)
        if (refresh) {
            refetch()
        }
    }

    useEffect(() => {
        if (data && data.payload) {
            setWords(data.payload)
        } else {
            setWords([])
        }
    }, [data])

    return (
        <Main>
            <Head>
                <title>Word</title>
            </Head>
            <div className={"p-4 font-sans w-full max-w-4xl mx-auto"}>
                <div className={"mb-4 flex justify-between items-center"}>
                    <div className={"text-2xl"} >Word</div>
                    <div className={" bg-green-400 rounded flex items-center px-4 py-2"} onClick={() => onClickOverlay()}>
                        <div className={"text-white font-bold"} >Create</div>
                    </div>
                </div>
                {isLoading ? (
                    <div className={"flex justify-center p-12"}>
                        <AiOutlineLoading3Quarters className={"animate-spin"} size={"4em"} />
                    </div>
                ) : (
                    <div className={"mb-4"}>
                        {words.length === 0 ? (
                            <div>No Data</div>
                        ) : (
                            <div className={""}>
                                {words.map((word, key) => {
                                    return (
                                        <div key={key} className="border-b-2 py-4 mb-2" onClick={() => onClickOverlay(word.wordId)}>
                                            <div className={"text-xs"}>{word.kana}</div>
                                            <div className={"text-lg"}>{word.word}</div>
                                            <div className="flex flex-col mb-2">
                                                {word.means.map((mean, key) => {
                                                    return (
                                                        <div key={key}>{(key + 1) + ". " + mean}</div>
                                                    )
                                                })}
                                            </div>
                                            {word.kanjis.length !== 0 && (
                                                <div className={"flex flex-wrap mb-2"}>
                                                    {word.kanjis.map((kanji, key) => {
                                                        return (
                                                            <div key={key} className={"flex justify-center items-center w-10 h-10 shadow-md mr-4 rounded-sm text-lg bg-gray-200 mb-2"}>
                                                                <Link key={key} href={`/kanji/${kanji}`} passHref >
                                                                    <div>{kanji}</div>
                                                                </Link>
                                                            </div>
                                                        )
                                                    })}
                                                </div>
                                            )}
                                            <div className={"text-xs whitespace-pre-wrap"}>{word.description}</div>
                                        </div>
                                    )
                                })}
                            </div>
                        )}
                    </div>
                )}
                <ModalCreateWord
                    show={show}
                    onClickOverlay={onClickOverlay}
                    selectedId={selectedId}
                />
            </div>
        </Main>
    )
}

export default Word