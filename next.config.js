module.exports = {
  reactStrictMode: true,
  env: {
    // LOCAL_END_POINT: "http://localhost:3001",
    APP_NAME: "Next Kanji",
    LOCAL_END_POINT: "http://192.168.100.212:3001",
    // API_END_POINT: "https://192.168.100.212:1323"
    API_END_POINT: "https://api.nextkanji.com"
  }
}
